Source: libjchart2d-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Markus Koschany <apo@debian.org>
Build-Depends:
 ant,
 debhelper-compat (= 13),
 default-jdk,
 default-jdk-doc,
 javahelper,
 junit4,
 libjide-oss-java,
 libxmlgraphics-commons-java,
 maven-repo-helper (>= 1.6~)
Standards-Version: 4.5.1
Homepage: http://jchart2d.sourceforge.net/
Vcs-Git: https://salsa.debian.org/java-team/libjchart2d-java.git
Vcs-Browser: https://salsa.debian.org/java-team/libjchart2d-java

Package: libjchart2d-java
Architecture: all
Depends:
 ${java:Depends},
 ${misc:Depends}
Recommends:
 ${java:Recommends}
Suggests:
 libjchart2d-java-doc
Description: Java library for precise 2D charting visualizations
 JChart2D is a minimalistic charting library. It is designed for displaying
 multiple traces consisting of tracepoints, including dynamic (animated) data.
 JChart2D is centered around a single configurable swing widget: the Chart2D.
 This library is intended for engineering tasks and not for presentations.

Package: libjchart2d-java-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends}
Recommends:
 default-jdk-doc,
 ${java:Recommends}
Description: Java library for precise 2D charting visualizations -- docs
 JChart2D is a minimalistic charting library. It is designed for displaying
 multiple traces consisting of tracepoints, including dynamic (animated) data.
 JChart2D is centered around a single configurable swing widget: the Chart2D.
 This library is intended for engineering tasks and not for presentations.
 .
 This package contains the Javadoc API documentation for libjchart2d-java.
